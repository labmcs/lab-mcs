#! /usr/bin/python3

def runge_kutta(f, x, v, t, step, tolerance = 0.01):

    def step_rk4(f, x, v, t, step):
        k1 = step * f(x, v, t)
        w1 = step * v
        k2 = step * f(x + k1/2, v + w1/2, t + step/2)
        w2 = step * (v + w1/2)
        
        x_next = x + w2
        v_next = v + k2
        
        return x_next, v_next

    while True:
        
        x_rk2, v_rk2 = step_rk2(f, x, v, t, step)

        x_rk2_half, v_rk2_half = step_rk2(f, x, v, t, step/2)
        x_rk2_half, v_rk2_half = step_rk2(f, x_rk2_half, v_rk2_half, t + step/2, step/2)

        error = np.abs(x_rk2 - x_rk2_half)

        if error > tolerance:
            step = step / 2
        else:
            break

    x_next, v_next = step_rk2(f, x, v, t, step)
    return x_next, v_next