import numpy as np
import ROOT

from scipy.constants import pi

def main():
    
    rnd = ROOT.TRandom3(10930234)

    N_SAMPLES = 1_000_000
    SPACE_DIMENSION = 10

    RADIUS = 1

    for dimension in range(SPACE_DIMENSION):
        inside_points = 0
        for i in range(N_SAMPLES):
            modulus = 0
            for i in range(dimension):
                modulus += (rnd.Rndm())**2
            if np.sqrt(modulus) <= RADIUS:
                inside_points += 1

        alpha = inside_points/N_SAMPLES * ((2 * RADIUS)/RADIUS)**dimension
        alpha_theo = np.pi**(dimension/2) / ROOT.TMath.Gamma(dimension / 2 + 1)
        print(f'n = {dimension}, α/α_t = {alpha} / {alpha_theo}')
                    

if __name__ == "__main__":
    main()
