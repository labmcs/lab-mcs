import numpy as np
import ROOT

def main():
    N_OBS = 30
    N_BKG = 15

    p = 0.05
    i = 0
    while True:
        prob = 0
        for j in range(N_OBS):
            prob += ROOT.TMath.PoissonI(j, i+N_BKG)
        if prob < p:
            print(f'Upper Limit @ 95% CLs N = {i-1}')
            if (N_OBS>i-1):
                print('Null HP rejected')
            break

        i += 1
        
if __name__ == '__main__':
    main()
