#! /usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

def sor(U, C, delta, omega):
    '''Return Successive Over-Relaxation method

    Parameters
    ----------
    U: `np.ndarray`
    potential scalar-field 
    C: `np.ndarray`
    density scalar-field

    Returns
    -------
    U: `np.ndarray`
    relaxed scalar field
    '''

    I, J = np.shape(U)

    r = np.zeros_like(U)
    
    U_old = np.zeros_like(U)
    U_old[:,:] = U[:,:]

    for i in range(1, I-2):
        for j in range(1, J-2):
            r[i, j] = (
                1 / 4
                *
                (
                    U[i+1, j]
                    +
                    U[i-1, j]
                    +
                    U[i, j+1]
                    +
                    U[i, j-1]
                )
                +
                1 / 4
                *
                C[i, j]
                *
                delta**2
                - 
                U_old[i, j]
            )

            U[i,j] = U_old[i,j] + omega * r[i,j]
            
    return U


def convergence(U_n0: np.ndarray, U_n1: np.ndarray, epsilon_a: float, epsilon_r: float, iteration):

    delta_ij = np.abs(U_n1 - U_n0)
    test = delta_ij < epsilon_a * np.ones_like(U_n0) + np.abs(U_n1) * epsilon_r
    
    if test.all() == True:
        print('\nconverged in %d iterations'%(iteration))
        return True

    return False


def main():
    
    N_SAMPLE_SIZE = 100
    CHARGE = 100
    EPSILON = 5
    
    potential = np.zeros(
        (
            N_SAMPLE_SIZE,
            N_SAMPLE_SIZE
        )
    )

    charge_density = np.zeros_like(potential)
    potential_previous  = np.ones_like(potential)*CHARGE
    potential[0, :] = 0
    potential[-1, :] = 0
    potential[:, 0] = 0
    potential[:, -1] = 0

    charge_density[
        int(N_SAMPLE_SIZE/2)-2:int(N_SAMPLE_SIZE/2)+2,
        int(N_SAMPLE_SIZE/2)-2:int(N_SAMPLE_SIZE/2)+2
    ] = - CHARGE
    
    for i in range(N_SAMPLE_SIZE):
        for j in range(N_SAMPLE_SIZE):
            if abs((i-N_SAMPLE_SIZE/2)**2 + (j-N_SAMPLE_SIZE/2)**2 - N_SAMPLE_SIZE**2/16) < EPSILON**2:
                if i > N_SAMPLE_SIZE/2:
                    charge_density[i, j] = 2 * CHARGE

    for i in range(N_SAMPLE_SIZE):
        for j in range(N_SAMPLE_SIZE):
            if abs((i-N_SAMPLE_SIZE/2)**2 + (j-N_SAMPLE_SIZE/2)**2 - N_SAMPLE_SIZE**2/25) < EPSILON**2:
                if i < N_SAMPLE_SIZE/2:
                    charge_density[i, j] = - CHARGE

    
    x_coordinates = np.linspace(-1, 1, N_SAMPLE_SIZE)
    delta = np.abs(x_coordinates[1] - x_coordinates[0])
    y_coordinates = np.linspace(-1, 1, N_SAMPLE_SIZE)
    x_coordinates_mesh, y_coordinates_mesh = np.meshgrid(
        x_coordinates,
        y_coordinates
    )

    iteration = 0
    
    while not convergence(potential, potential_previous, 0.000001, 0.000001, iteration):

        potential_previous[:, :] = potential[:, :]
        potential = sor(potential, charge_density, delta, 1.8)
        potential[0, :] = 0
        potential[-1, :] = 0
        potential[:, 0] = 0
        potential[:, -1] = 0

        print('\riteration: %d'%iteration, end='')
        iteration += 1

    plt.figure()
    plt.contour(
        x_coordinates_mesh,
        y_coordinates_mesh,
        potential,
        cmap = 'bwr',
        levels=50,
        norm = mpl.colors.CenteredNorm(0),
    )

    plt.colorbar()

    E_y, E_x = np.gradient(potential)

    plt.streamplot(
        x_coordinates_mesh,
        y_coordinates_mesh,
        E_x,
        E_y,
        color='k'
    )
    
    plt.show()
    
if __name__ == '__main__':
    main()
