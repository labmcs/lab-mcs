import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from tqdm import tqdm 

def crank_nicolson(U, eta):

    N = len(U)

    heat_propagation_matrix = (
        - np.diag(
            np.ones(N-3),
            1
        )
        +
        np.diag(
            np.ones(N-2)*(2/eta + 2)
        )
        -
        np.diag(
            np.ones(N-3),
            -1
        )
    )

    inverse_heat_propagation_matrix = np.linalg.inv(heat_propagation_matrix)
    
    U_tmp = (
        U[0:-2]
        +
        U[1:-1] * (
            2 / eta
            -
            2
        )
        +
        U[2:]
    )

    U_tmp[0] += BASE_TEMPERATURE
    U_tmp[-1] += BASE_TEMPERATURE

    U_next = np.copy(U)
    U_next[1:-1] = np.matmul(
        inverse_heat_propagation_matrix,
        U_tmp
    )
    
    return U_next

if __name__ == '__main__':

    N_SAMPLES = 101
    N_ITERATIONS = 1_000

    BASE_TEMPERATURE = 20
    PEAK_TEMPERATURE = 330

    ETA = 0.2
    KAPPA = 10 
    BAR_LENGHT = 0.5
    
    HEAT = np.ones(
        (N_ITERATIONS, N_SAMPLES)
    ) * BASE_TEMPERATURE

    HEAT[0, int(N_SAMPLES/2+1)-1] += PEAK_TEMPERATURE

    x_coordinate = np.linspace(0, BAR_LENGHT, N_SAMPLES)
    interval_x = x_coordinate[1] - x_coordinate[0]
    coordinates_time = np.zeros(N_ITERATIONS)
    interval_time = (ETA*interval_x**2)/KAPPA 
    simulation_time = 0

    for i in tqdm(range(1, N_ITERATIONS)):

        HEAT[i] = crank_nicolson(HEAT[i-1], ETA)
        coordinates_time[i] = simulation_time
        simulation_time += interval_time

    plt.figure()
    
    x_mesh, t_mesh = np.meshgrid(
        x_coordinate,
        coordinates_time
    )

    plt.contourf(
        x_mesh,
        t_mesh,
        np.log(HEAT),
        cmap='afmhot',
        levels=100
    )
    plt.colorbar()
    plt.show()
