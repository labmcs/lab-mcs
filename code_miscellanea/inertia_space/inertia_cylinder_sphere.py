import numpy as np
import ROOT

from tqdm import tqdm
from uncertainties import ufloat

def main():

    SPHERE_RADIUS = 1
    CYLINDER_RADIUS = 1
    CYLINDER_HEIGHT = 2
    MATERIAL_DENSITY = 1

    MC_SAMPLES: float = 1_000_000
    
    rnd = ROOT.TRandom3(987654321)

    x_domain = (
        2 * SPHERE_RADIUS
        +
        2 * CYLINDER_RADIUS
    )

    y_domain = (
        2 * np.max([
            SPHERE_RADIUS,
            CYLINDER_RADIUS
        ])
    )

    z_domain = (
        np.max([
            2 * SPHERE_RADIUS,
            CYLINDER_HEIGHT
        ])
    )

    integration_domain_volume = (    
        x_domain * y_domain * z_domain
    )

    points_in_volume = 0
    distance_square_sum = 0
    
    axis_x = CYLINDER_RADIUS
    axis_y = y_domain / 2
    half_domain_z = z_domain * 0.5

    sphere_center_x = 2 * CYLINDER_RADIUS + SPHERE_RADIUS 
    sphere_center_y = axis_y
    sphere_center_z = half_domain_z

    for _ in tqdm(range(MC_SAMPLES)):

        x = x_domain * rnd.Rndm()
        y = y_domain * rnd.Rndm()
        z = z_domain * rnd.Rndm()

        if ((x - axis_x)**2 + (y - axis_y)**2) < CYLINDER_RADIUS**2:
            if np.abs(z - half_domain_z) < CYLINDER_HEIGHT * 0.5:
                points_in_volume += 1
                distance_square_sum += (
                    (x - axis_x)**2 +  (y - axis_y)**2
                )

        point_position_sphere = np.sqrt(
            (x - sphere_center_x)**2
            +
            (y - sphere_center_y)**2
            +
            (z - sphere_center_z)**2
        )
        
        if point_position_sphere < SPHERE_RADIUS:
            points_in_volume += 1
            distance_square_sum += (
                (x - axis_x)**2 +  (y - axis_y)**2
            )

    volume = points_in_volume / MC_SAMPLES * integration_domain_volume
    MC_inertia = MATERIAL_DENSITY * volume * distance_square_sum / points_in_volume

    p = points_in_volume / MC_SAMPLES
    
    MC_inertia_error = (
        (
            MATERIAL_DENSITY
            *
            distance_square_sum
            *
            integration_domain_volume
            /
            points_in_volume
        )
        *
        np.sqrt(p * (1 - p) / MC_SAMPLES)
    )
    analytical_inertia = (
        (
            0.5 * MATERIAL_DENSITY
            *
            (
                np.pi * CYLINDER_RADIUS**2 * CYLINDER_HEIGHT
            )
            *
            CYLINDER_RADIUS**2
        )
        +
        (
            (
                4. / 3 * np.pi * SPHERE_RADIUS**3
            )
            *
            2. / 5 * MATERIAL_DENSITY * SPHERE_RADIUS**2
        )
        +
        (
            (
                4. / 3 * np.pi * SPHERE_RADIUS**3
            )
            *
            MATERIAL_DENSITY
            *
            (
                SPHERE_RADIUS + CYLINDER_RADIUS
            )**2
        )
    )

    result = f'''\n***\
    \nMC samples         {MC_SAMPLES}\
    \nintegration volume {integration_domain_volume} m^3\
    \nobject volume      {volume} m^3\
    \nobject inertia     {ufloat(MC_inertia, MC_inertia_error):uS} kg m^2\
    \nanalytical inertia {analytical_inertia:.8} kg m^2\n'''

    print(result)
    

    
    

if __name__ == '__main__':
    main()
