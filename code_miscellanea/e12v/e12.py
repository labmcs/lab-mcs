import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import mplhep as hep

import ROOT

import scipy.stats

mpl.style.use(['dark_background', 'science', 'nature'])

from tqdm import tqdm

def main():

    N_SAMPLES = 500_000
    MU = 150
    S: float = 0
    
    rnd = ROOT.TRandom3(3902084)

    h_reco = ROOT.TH1D('h_reco', '', 45, 0, 0)
    h_reco_p = ROOT.TH1D('h_reco_p', '', 45, 0, 0)
    h_poisson = ROOT.TH1D('h_poisson', '', 14, 0, 0)

    def get_pdf(cdf):
        return np.log(1 / (1 - cdf))

    n = 0
    poisson_MC = np.array([])
    
    for i in tqdm(range(N_SAMPLES)):

        x = get_pdf(rnd.Rndm())
        h_reco.Fill(x)
        S += x

        if S > MU:
            S = 0
            h_poisson.Fill(i-n-1)
            h_reco_p.Fill(x)
            poisson_MC = np.append(poisson_MC, i-n-1)
            n = i

    norm = h_poisson.GetEntries() * h_poisson.GetBinWidth(1)
    
    fig, (ax1, ax2) = plt.subplots(1,2,figsize=(8,4.5))
    poisson = scipy.stats.poisson(MU)

    hep.histplot(h_reco, yerr=False, color='w', ax=ax1)
    hep.histplot(h_reco_p, yerr=False, color='r', histtype='fill', ax=ax1)
    hep.histplot(h_poisson, color='w', ax=ax2, label='Poisson MC')
    D, p_value = scipy.stats.kstest(poisson_MC, poisson.cdf)
    x_range = np.array(np.sort(poisson_MC))
    ax2.plot(
        x_range,
        norm * poisson.pmf(x_range),
        color='r',
        label=f'KS p-value  {p_value:.2}\nPoisson($\\nu={MU}$)'
    )

    print(f'p-value = {p_value}')

    ax2.legend(loc='upper right')
    
    plt.show()
    fig.show()
    
    
        

if __name__ == '__main__':
    main()
