#include "zeroTF1.h"
#include <iostream>

int main(int argc, const char **argv){

	zeroTF1 *f = new zeroTF1("f", "sin(x)");

	double zeroNULL = f->FindZero(0.5, 1);
	double zero = f->FindZero(-1, 1);
	double zeroPI = f->FindZero(2, 4);

	std::cout << "\n** sin(x) \n"
			  << "zero in [0.5, 1] => " << zeroNULL << "\n"
			  << "zero in [-1., 1] => " << zero << "\n"
			  << "zero in [2., 4.] => " << zeroPI
			  << std::endl;

	zeroTF1 *f2 = new zeroTF1("f", "x*x*x-1");

	double zeroNULL2 = f2->FindZeroI(0.5, 1.1);
	double zero2 = f2->FindZeroI(-1, 1.1);
	double zeroPI2 = f2->FindZeroI(2, 4);

	std::cout << "\n** x^3-1 \n"
			  << "zero in [0.5, 1.1] => " << zeroNULL2 << "\n"
			  << "zero in [-1, 1.1]  => " << zero2 << "\n"
			  << "zero in [2., 4.]   => " << zeroPI2
			  << std::endl;
	
	return 0;
}

