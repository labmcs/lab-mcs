#ifndef ZEROTF1_H
#define ZEROTF1_H

#include <TF1.h>
#include <iostream>
#include <cmath>

class zeroTF1: public TF1
{
public:
	using TF1::TF1;
	double FindZero(double xmin = 0, double xmax = 1, double tolerance = 1e-12);
	double FindZeroI(double xmin = 0, double xmax = 1, double tolerance = 1e-12);
};

#endif
