#include "zeroTF1.h"

double zeroTF1::FindZero(double xmin, double xmax, double tolerance){

	if(xmin > xmax){
		double tmp = xmin;
		xmin = xmax;
		xmax = tmp;
	}
	
	double _x0 = xmax;

	if(this->Eval(xmin) * this->Eval(xmax) > 0){
		std::cerr << "\n\033[0;31mwarning\033[0m: ["
				  << xmin << ", " << xmax << "] extreme points are same sign! Returning 0!"
				  << std::endl;
		return NULL;
	}
	
	while(true){
		double _x0_next = _x0 - this->Eval(_x0) / this->Derivative(_x0);
		if(abs(this->Eval(_x0_next) - this->Eval(_x0)) < tolerance)
			return _x0_next;
		else
			_x0 = _x0_next;
	}
	
	return _x0;
}

double zeroTF1::FindZeroI(double xmin, double xmax, double tolerance){

	if(xmin > xmax){
		double tmp = xmin;
		xmin = xmax;
		xmax = tmp;
	}

	if(this->Eval(xmin) * this->Eval(xmax) > 0){
		std::cerr << "\n\033[0;31mwarning\033[0m: ["
				  << xmin << ", " << xmax << "] extreme points are same sign! Returning 0!"
				  << std::endl;
		return NULL;
	}

	double p, _xmed;
	double phi = (1 + sqrt(5))/2;
	
	while(true){
		_xmed = (xmax+xmin) / 2;
		
		if(this->Eval(_xmed) == 0)
			return _xmed;
		
		p = this->Eval(xmin) * this->Eval(_xmed);
		if(p<0)
			xmax = _xmed;
		else
			xmin = _xmed;
		
		if((xmax-xmin) < tolerance)
			return (xmax + xmin) / 2;
	}
	return 0;
}
