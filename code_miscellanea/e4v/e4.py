import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import mplhep as hep

import ROOT
from ROOT import TTree, TH1D, TF1

plt.style.use(hep.style.ATLAS)

def main():
    t = TTree()
    t.ReadFile('DatiGamma.dat', 't/D')
    h = TH1D('h', '', 55, 0, 0)
    t.Draw('t>>h')
    f = TF1('f', '[1]*x*x*exp(-x/[0])/(2*[0]*[0]*[0])', 0, 100)
    f.SetParameter(0, 2);
    f.SetParameter(1, 200 * 1)
    h.Fit('f', 'L')
    h.Draw('E0')
    ROOT.gApplication.Run(True)

if __name__ == '__main__':
    ROOT.gROOT.SetStyle('ATLAS')
    ROOT.gStyle.SetOptStat(0)
    main()
