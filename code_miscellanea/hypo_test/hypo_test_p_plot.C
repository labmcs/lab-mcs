void CL(float nbkg, int nobs, double p = 0.05){
	std::cout << "n_s (n_obs - n_bkg) = " << nobs - nbkg
			  << ", n_obs = "<< nobs << std::endl;
	//////////////////////////////// Upper and lower limit for 95% CLs 
	for (unsigned int ns=0;; ns++){
		double prob=0;
		for(unsigned i=0; i<=nobs; i++)
			prob += TMath::PoissonI(i, nbkg + ns);
		if (1-prob > p){ // Lower limit for 95% CLs
			std::cout << "** lower limit at 95% CL: " << ns-1 << std::endl; break;
		}
	}
	for (unsigned int ns=0;; ns++){
		double prob=0;
		for(unsigned int i=0; i<=nobs; i++)
			prob += TMath::PoissonI(i, nbkg + ns);
		if (prob < p){ // Upper limit for 95% CLs
			std::cout << "** upper limit at 95% CL: " << ns-1 << std::endl; break;
		}
	}
	//////////////////////////////// Upper-lower limit for 95% CLs (ULL)
	for (unsigned int ns=0;; ns++){
		double prob=0;
		for(unsigned int i=0; i<=nobs; i++)
			prob += TMath::PoissonI(i, nbkg + ns);
		if (1-prob > p/2){
			std::cout << "** ULL at 95% CL: (" << ns-1; break;
		}
	}
	for (unsigned int ns=0;; ns++){
		double prob=0;
		for(unsigned int i=0; i<=nobs; i++)
			prob += TMath::PoissonI(i, nbkg + ns);
		if (prob < p/2){
			std::cout << ", " << ns-1 << ")" << std::endl; break;
		}
	}
}

double p_value(double n_bkg, int n_obs){
	double prob=0;
	for(unsigned i=0; i<n_obs; i++)
		prob+=TMath::PoissonI(i, n_bkg);
	return 1-prob;
}

void draw_vline(double x, double ymax, Color_t color = kBlack){
	auto *l = new TLine(x, 0, x, ymax);
	l->SetLineStyle(kDashed);
	l->SetLineColor(color);
	l->Draw();
}

void draw_hline(double y, double xmin, double xmax, Color_t color = kRed){
	auto *l = new TLine(xmin, y, xmax, y);
	l->SetLineStyle(kDashed);
	l->SetLineColor(color);
	l->Draw();
}

void draw_p_VL(double xmin, double xmax, int sigmamax){
	auto *t = new TLatex();
	t->SetTextSize(0.035);
	t->SetTextColor(kRed);
	for (unsigned i=1; i<=sigmamax; i++) {
		draw_hline(1 - TMath::Freq(i), xmin, xmax);
		t->DrawLatex(xmax+4, (1 - TMath::Freq(i)), Form("%d #sigma",i));
	}
}

void hypo_test_p_plot(){
	atlasStyle();
	gStyle->SetOptStat(0);
	auto *c = new TCanvas("","", 650, 1000);
	c->Divide(1,2);
	double data;
	double bkg, peak;
	long n_samples = 55500;
	double lambda = 0.015;
	double mu = 165, sigma = 7.5;
	unsigned bins = 55;
	double xmin = 75, xmax = 250;
	double binomial_simulation = 0.01;
	auto *rnd = new TRandom3(310591);
	auto *t = new TTree();
	t->Branch("t", &data, "t/D");
	auto *h_reco = new TH1D("h_reco", ";Data;Entries", bins, xmin, xmax);
	auto *h_peak = new TH1D("h_peak", ";Data;Entries", bins, xmin, xmax);
	h_peak->SetFillColor(kOrange);
	auto *h_bkg = new TH1D("h_bkg", ";Data;Entries", bins, xmin, xmax);
	h_bkg->SetFillColor(kPink-9);
	for (unsigned i = 0; i < n_samples; i++){
		if (rnd->Rndm()>binomial_simulation){
			bkg = - 1. / lambda * TMath::Log(1 - rnd->Rndm());
			data = bkg;
			t->Fill();
			h_bkg->Fill(bkg);
		} else {
			peak = rnd->Gaus(mu, sigma);
			data = peak;
			t->Fill();
			h_peak->Fill(peak);
		}
	}
	c->cd(1);
	t->Draw("t>>h_reco");
	double ymax = h_bkg->GetMaximum()*1.1;
	h_bkg->GetYaxis()->SetRangeUser(0, ymax);
	h_bkg->Draw();
	h_reco->Draw("SAME E0");
	h_peak->Draw("SAME");

	auto *f_reco = new TF1("f_reco",
						   "[0]*([1]*TMath::Gaus(x, [2], [3], 1)+(1-[1])*[4]*exp(-[4]*x))");
	f_reco->SetParameters(h_reco->GetEntries()*h_reco->GetBinWidth(1),
						  binomial_simulation, mu, sigma, lambda);
	auto *f_gaus = new TF1("f_gaus", "[0]*TMath::Gaus(x,[1],[2],1)", xmin, xmax);
	f_gaus->SetLineColor(kCyan-5);
	h_reco->Fit("f_reco", "L");
	double mu_gaus = f_reco->GetParameter(2);
	double sigma_gaus = f_reco->GetParameter(3);
	f_gaus->SetParameters(f_reco->GetParameter(0)*f_reco->GetParameter(1),
						  mu_gaus, sigma_gaus);
	f_gaus->Draw("SAME");
	std::cout << "\n** p-value for combined model = " << f_reco->GetProb() << std::endl;

	auto *f_bkg = new TF1("f_bkg", "[0]*[1]*exp(-[1]*x)", xmin, xmax);
	f_bkg->SetParameters(f_reco->GetParameter(0)*(1-f_reco->GetParameter(1)),
						 f_reco->GetParameter(4));
	h_bkg->Fit("f_bkg", "L 0");
	f_bkg->SetLineStyle(kDashed);
	f_bkg->Draw("SAME");
	double CLmin = mu_gaus - 3 * sigma_gaus, CLmax = mu_gaus + 3 * sigma_gaus;
	draw_vline(CLmin, ymax);
	draw_vline(CLmax, ymax);
	float n_bkg = f_bkg->Integral(CLmin, CLmax)/h_bkg->GetBinWidth(1);
	unsigned n_obs = 0;
	int min_obs = h_reco->FindBin(CLmin), max_obs = h_reco->FindBin(CLmax);
	for(unsigned i=min_obs; i<max_obs+1; i++)
		n_obs += h_reco->GetBinContent(i);
	std::cout << "n_bkg = " << n_bkg;
	std::cout << std::endl;
	double prob = 0.05; // accepting values @ 95% CL
	CL(n_bkg, n_obs, prob);
	
	auto *legend = new TLegend(0.65, 0.65, 0.925, 0.9);
	legend->SetLineWidth(0);
	legend->AddEntry(h_bkg, "Exp. background", "F");
	legend->AddEntry(h_peak, "Signal", "F");
	legend->AddEntry(h_reco, "Data", "EP");
	legend->AddEntry(f_reco, "Model", "L");
	legend->AddEntry(f_gaus, "Gaus. signal", "L");
	legend->Draw();

	c->cd(2);
	auto *pG = new TGraph(h_reco->GetNbinsX());
	double n_bkg_p = 0, n_obs_p = 0;
	for (unsigned i=1; i<=h_reco->GetNbinsX(); i++) {
		n_bkg_p = f_bkg->Integral(h_reco->GetBinLowEdge(i),
								  h_reco->GetBinLowEdge(i)+h_reco->GetBinWidth(i))
			/ h_reco->GetBinWidth(i);
		n_obs_p = h_reco->GetBinContent(i);
		pG->SetPoint(i-1, h_reco->GetBinCenter(i), p_value(n_bkg_p, n_obs_p));
	}
	
	pG->GetXaxis()->SetRangeUser(xmin, xmax);
	pG->GetYaxis()->SetRangeUser(1e-8, 100);
	pG->SetTitle(";Data;Local p_{0}");
	gPad->SetLogy();
	pG->Draw("AL");
	draw_hline(1, xmin, xmax, kBlack);
	draw_p_VL(xmin, xmax, 5);

	auto *l2 = new TLegend(0.15, 0.825, 0.55, 0.875);
	l2->SetLineWidth(0);
	l2->AddEntry(pG, "Local p_{0} value (MC dataset)", "L"); 
	l2->Draw();



	c->Print("plot.pdf");
}


