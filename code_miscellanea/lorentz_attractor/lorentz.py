import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy

import mplhep as hep

mpl.style.use(['science', 'nature', 'dark_background'])

from tqdm import tqdm

def runge_kutta(fcn, x, t, step, tolerance = 0.001):

    t_step = step
    
    def rk4(fcn, x, t, step):
        k1 = step * fcn(x, t)
        k2 = step * fcn(x + k1/2, t + step/2)
        k3 = step * fcn(x + k1/2, t + step/2)
        k4 = step * fcn(x + k3, t + step)
        
        return x + k1/6 + k2/3 + k3/3 + k4/6

    while True:

        x_2h = rk4(fcn, x, t, t_step)
        x_hh = rk4(fcn, x, t, t_step/2)
        
        x_hh = rk4(fcn, x_hh, t + t_step/2, t_step/2)

        error = np.abs(x_2h - x_hh)

        if error.any() > tolerance:
            t_step = t_step/2
        else:
            break

    x_next = rk4(fcn, x, t, step)
    return x_next


def attractor(x0, y0, z0):
    SIGMA = 10
    RHO = 28
    BETA = 8./3

    N_ITERATIONS = 500_000

    x_coordinates = np.zeros((3, N_ITERATIONS))
    x_coordinates[:, 0] = [x0, y0, z0]
    time_coordinates = np.linspace(0, 100, N_ITERATIONS)
    time_step = time_coordinates[1] - time_coordinates[0]
    
    def lorentz(x, t):
        dx_dt = np.array(
            [
                SIGMA * (x[1] - x[0]),
                x[0] * (RHO - x[2]) - x[1],
                x[0] * x[1] - BETA * x[2]
            ]
        )
        return dx_dt

    for i in tqdm(range(1, N_ITERATIONS-1)):
        x_coordinates[:, i] = runge_kutta(
            lorentz,
            x_coordinates[:, i-1],
            time_coordinates[i-1],
            time_step
        )

    return x_coordinates, time_coordinates

def main():

    x1, t1 = attractor(1, 1, 1)
    x2, t2 = attractor(1.0000001, 0.99999995, 1)
    x3, t3 = attractor(0.9999997, 1, 0.9999992)

    lw = 0.35
    
    fig = plt.figure(figsize=(7,7))
    grid = fig.add_gridspec(2, 2, left=0.1, right=0.9, bottom=0.1, top=0.9,
                            wspace=0.05, hspace=0.05)
    ax_xy = fig.add_subplot(grid[1,0])
    ax_xy.plot(
        x1[0],
        x1[1],
        'w',
        x2[0],
        x2[1],
        'r',
        x3[0],
        x3[1],
        'g',
        lw=lw
    )
    ax_xy.set_xlabel(r'$x$')
    ax_xy.set_ylabel(r'$y$')
    ax_xz = fig.add_subplot(grid[0,0], sharex = ax_xy)
    ax_xz.plot(
        x1[0],
        x1[2],
        'w',
        x2[0],
        x2[2],
        'r',
        x3[0],
        x3[2],
        'g',
        lw=lw
    )
    ax_xz.set_ylabel(r'$z$')
    ax_xz.tick_params(labelbottom=False)
    ax_zy = fig.add_subplot(grid[1,1], sharey = ax_xy)
    ax_zy.plot(
        x1[2],
        x1[1],
        'w',
        x2[2],
        x2[1],
        'r',
        x3[2],
        x3[1],
        'g',
        lw=lw
    )
    ax_zy.set_xlabel(r'$z$')
    ax_zy.tick_params(labelleft=False)
    # plt.show()
    fig.savefig('plot_lorentz.pdf', bbox_inches='tight')

    fig, ax = plt.subplots(figsize=(7,7))
    ax.plot(
        x1[0],
        x1[2],
        'w',
        x2[0],
        x2[2],
        'r',
        x3[0],
        x3[2],
        'c',
        lw=lw
    )
    ax.axis('off')
    # plt.show()
    fig.savefig('../figures/lorentz_attractor.pdf', bbox_inches = 'tight', transparent=True)


if __name__ == '__main__':
    main()
    
