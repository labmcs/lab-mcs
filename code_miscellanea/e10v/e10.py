
def eulero_cromer(fcn, v, x, t, step):
    '''
    Metodo di Eulero-Cromer,
    assumo fcn = fcn(v,x,t)
    '''
    
    x_next = x + v * step
    v_next = v + fcn(v, x_next, t) * step

    return x_next, v_next

def runge_kutta(f, v, x, t, step, tolerance = 0.01):

    def rk4(f, v, x, t, step):
        k1 = step * f(v, x, t)
        w1 = step * v
        k2 = step * f(v + k1/2, x + w1/2, t + step/2)
        w2 = step * (v + k1/2)
        k3 = step * f(v + k2/2, x + w2/2, t + step/2)
        w3 = step * (v + k1/2)
        k4 = step * f(v + k3/2, x + w3/2, t + step/2)
        w4 = step * (v + k1/2)
        
        x_next = x + w1/6 + w2/3 + w3/3 + w4/6
        v_next = v + k1/6 + k2/3 + k3/3 + k4/6
        
        return x_next, v_next

    '''
    while True:
        
        x_2h, v_2h = rk4(f, v, x, t, step)

        x_hh, v_hh = rk4(f, v, x, t, step/2)
        x_hh, _ = rk4(f, v_hh, x_hh, t + step/2, step/2)

        error = np.abs(x_2h - x_hh)

        if error.all() > tolerance:
            step = step / 2
        else:
            break
    '''


    x_next, v_next = rk4(f, v, x, t, step)
    return x_next, v_next


def main():

    import numpy as np
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    plt.style.use(['dark_background'])

    EARTH_MASS: float = 6e24*6.67e-11 #float(input('M = '))
    x_start: float = 4e5 #float(input('x_0 = '))
    vy_start: float = 3e4 #float(input('v_y0 = '))
    sim_step: float = 0.01 #float(input('simulation_step = '))
    sim_iterations: int = 10_000 #int(input('simulation_iterations = '))

    def central(v, x, t):
        return - EARTH_MASS * x / np.sqrt(x[0]**2 + x[1]**2)**3

    x = np.zeros((sim_iterations, 2))
    v = np.zeros((sim_iterations, 2))
    x[0] = [x_start, 0]
    v[0] = [0, vy_start]
    t = np.zeros(sim_iterations)

    # ## Di seguito anche implementata con verlet position
    # x[1], v[1] =  eulero_cromer(central, v[0], x[0], t[0], sim_step)

    for i in range(0, sim_iterations-1):
        
        # x[i+1], v[i+1] = eulero_cromer(central, v[i], x[i], t[i], sim_step)

        # ## Di seguito anche implementata con verlet position
        # x[i+1] = 2 * x[i] - x[i-1] + sim_step**2 * central(None, x[i], t[0])

        ## Infine anche l'implementazione con Verlet Velocity

        x[i+1] = (
            x[i] + sim_step * v[i]
            +
            sim_step**2 / 2 * central(None, x[i], t[i])
        )
        v[i+1] = (
            v[i]
            +
            sim_step * (central(None, x[i], t[i]) + central(None, x[i+1], t[i+1])) / 2
        )
        
        t[i+1] = i * sim_step
        print(f'\riteration {i}, {x[i]}', end='')

    plt.plot(x[:,0], x[:,1], 'r')
    plt.show()


if __name__ == '__main__':
    main()
