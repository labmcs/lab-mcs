import numpy as np
import scipy

import matplotlib as mpl
import matplotlib.pyplot as plt

from tqdm import tqdm


def G(a, v, x, eta):
    return -0.5 * a * x

def runge_kutta(f, a, v, x, eta, step, tolerance = 0.01):

    def step_rk2(f, a, v, x, eta, step):

        w1 = step * f(a, v, x, eta)
        k1 = step * a
        j1 = step * v

        w2 = step * f(a+w1/2, v+k1/2, x+j1/2, eta+step/2)
        k2 = step * (a + w1/2)
        j2 = step * (v + k1/2)
        
        x_next = x + j2
        v_next = v + k2
        a_next = a + w2
        
        return x_next, v_next, a_next

    while True:
        
        x_rk2, v_rk2, a_rk2 = step_rk2(f, a, v, x, eta, step)

        x_rk2_half, v_rk2_half, a_rk2_half = step_rk2(f, a, v, x, eta, step/2)
        x_rk2_half, v_rk2_half, a_rk2_half = step_rk2(f, a_rk2_half, v_rk2_half, x_rk2_half, eta + step/2, step/2)

        error = np.abs(x_rk2 - x_rk2_half)

        if error > tolerance:
            step = step / 2
        else:
            break

    x_next, v_next, a_next = step_rk2(f, a, v, x, eta, step)
    return x_next, v_next, a_next

def main():

    N_SAMPLE_SIZE = int(2**10)
    KINEMATIC_VISCOSITY = 1e-5
    VELOCITY = 10
    
    
    
    x_coordinates = np.linspace(0, 1, N_SAMPLE_SIZE)
    y_coordinates = np.linspace(0, 1, N_SAMPLE_SIZE)
    
    ETA = y_coordinates / np.sqrt(KINEMATIC_VISCOSITY * x_coordinates / VELOCITY)

    a = np.zeros(N_SAMPLE_SIZE)
    g = np.zeros(N_SAMPLE_SIZE)
    x = np.zeros(N_SAMPLE_SIZE)
    
    for i in tqdm(range(1, N_SAMPLE_SIZE-1)):

        a[i], g[i], x[i] = runge_kutta(G, a[i-1], g[i-1], x[i-1], ETA[i-1], 0.01)

    plt.figure()
    plt.plot(ETA, g * VELOCITY, color='r')
    plt.ylabel(r'$g(\eta)$')
    plt.xlabel(r'$\eta$')
    plt.show()

    print(ETA)
    
if __name__ == '__main__':
    main()
