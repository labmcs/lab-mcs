import numpy as np
import matplotlib as mpl

import matplotlib.pyplot as plt

def convergence(
        U_n0: np.ndarray,
        U_n1: np.ndarray,
        epsilon_a: float,
        epsilon_r: float, iteration
):
    delta_ij = np.abs(U_n1 - U_n0)
    test = delta_ij < epsilon_a * np.ones_like(U_n0) + np.abs(U_n1) * epsilon_r
    
    if test.all() == True:
        print('\nconverged in %d iterations'%(iteration))
        return True
    
    return False

def sor(U, C, delta, omega):
    '''Return Successive Over-Relaxation method
    Valid for a single iteration

    Parameters
    ----------
    U: `np.ndarray`
    potential scalar-field 
    C: `np.ndarray`
    density scalar-field

    Returns
    -------
    U: `np.ndarray`
    relaxed scalar field
    '''

    I, J = np.shape(U)

    r = np.zeros_like(U)
    
    U_old = np.zeros_like(U)
    U_old[:,:] = U[:,:]

    for i in range(1, I-2):
        for j in range(1, J-2):
            r[i, j] = (
                1 / 4
                *
                (
                    U[i+1, j]
                    +
                    U[i-1, j]
                    +
                    U[i, j+1]
                    +
                    U[i, j-1]
                )
                +
                1 / 4
                *
                C[i, j]
                *
                delta**2
                - 
                U_old[i, j]
            )

            U[i,j] = U_old[i,j] + omega * r[i,j]
            
    return U

def main():
    N_SAMPLES = 100
    DOMAIN_SIZE_BORDER = 1

    x_coordinates = np.linspace(0, DOMAIN_SIZE_BORDER, N_SAMPLES)
    y_coordinates = np.linspace(0, DOMAIN_SIZE_BORDER, N_SAMPLES)

    potential = np.zeros((N_SAMPLES, N_SAMPLES))
    
    


if __name__ == '__main__':
    main()



