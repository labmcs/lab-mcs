
import matplotlib as mpl
from matplotlib import pyplot as plt

import ROOT

import numpy as np
import scipy.stats
import mplhep as hep
mpl.style.use([hep.style.ATLAS, 'dark_background'])

def main():
    VALUE_X_0 = 10
    SIGMA = 2
    DELTA = 2
    N_SAMPLES = 10_000

    rnd = ROOT.TRandom3(123456789)
    
    h_reco = ROOT.TH1D("", "", 75, 0, 20)
    MC_values = np.zeros(N_SAMPLES)
    
    for i in range(N_SAMPLES):
        x_prime = rnd.Gaus(VALUE_X_0, SIGMA)

        x = x_prime + (rnd.Rndm() * 2 - 1) * DELTA
        h_reco.Fill(x)
        MC_values[i] = x

    norm_gaus = scipy.stats.norm(loc = VALUE_X_0, scale = np.sqrt(SIGMA**2 + DELTA**2/3))
    normalization = h_reco.GetEntries() * h_reco.GetBinWidth(1)
    x = np.linspace(h_reco.GetXaxis().GetXmin(), h_reco.GetXaxis().GetXmax(), 100)

    D, p_value = scipy.stats.kstest(MC_values, norm_gaus.cdf)
    
    plt.figure()
    hep.histplot(h_reco, histtype='errorbar', color='w')
    plt.plot(x, normalization * norm_gaus.pdf(x),
             'r-', label=f'Gaus (KS $p$-$value = {p_value:.3}$)')
    plt.xlabel('Value $x_0$')
    plt.ylabel('Entries')
    plt.legend()
    plt.savefig('plot.pdf', bbox_inches='tight')
    plt.show()
    

if __name__ == '__main__':
    main()
    
