import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from tqdm import tqdm

def verlet_velocity(fcn, v, x, t, step):
    '''
    Verlet Velocity next steps implementation
    Assuming fcn = fcn(v, x, t)
    '''

    x_next = x + step * v + step**2 / 2 * fcn(None, x, t)
    v_next = v + step / 2 * (fcn(None, x_next, t + step) + fcn (None, x, t))
    return x_next, v_next

def main():
    BODY_MASS = 1
    KAPPA = 0.25
    DRIVING_AMPLITUDE = 0.1
    DRIVING_FREQUENCY = 0.25

    TIME_STEP = 0.0001
    SIMULATION_TIME = 100
    simulation_iterations = int(SIMULATION_TIME / TIME_STEP) + 1

    position_space = np.zeros(simulation_iterations)
    position_space[0] = 1
    velocity_space = np.zeros(simulation_iterations)
    time_space = np.linspace(0, SIMULATION_TIME, simulation_iterations)

    def driver(v, x, t):
        return (DRIVING_AMPLITUDE * np.sin(DRIVING_FREQUENCY * t) - KAPPA * x) / BODY_MASS

    for i in tqdm(range(simulation_iterations - 1)):
        position_space[i+1], velocity_space[i+1] = verlet_velocity(
                driver, velocity_space[i], position_space[i], time_space[i], TIME_STEP
        )

    # import mplhep as hep
    # mpl.style.use([hep.style.ROOT, 'dark_background'])
    mpl.style.use(['dark_background', 'science', 'nature'])
    # fig, ax = plt.subplots(figsize=(14.5,6))
    fig, ax = plt.subplots(figsize=(6,3))
    ax.plot(time_space, position_space, color='r', label='Position')
    ax2 = ax.twinx()
    ax2.plot(time_space, velocity_space, 'w:', label='Speed')
    ax.set_xlabel('Time (s)')
    ax.set_ylabel('Position (m)')
    ax2.set_ylabel('Speed (m/s)')
    ax.legend(loc=(0,1), frameon=False)
    ax2.legend(loc=(0.2,1), frameon=False)
    plt.show()
    fig.savefig('plot.pdf', bbox_inches='tight')

    

if __name__ == '__main__':
    main()
