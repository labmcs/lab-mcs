#! /usr/bin/python3

import matplotlib as mpl
import matplotlib.pyplot as plt

import numpy as np

plt.style.use(['science', 'ieee', 'dark_background'])

def main():
    plt.figure()

    f = lambda x: x**3

    h = np.logspace(-15, 0, 1000)
    x = 1
    diff = np.array([(f(x + h[i]) - f(x - h[i])) / (2 * h[i]) - 3 for i in range(len(h))])

    plt.plot(h, diff, color='w')
    plt.xlabel(r'Value $h$')
    plt.ylabel(r'Precision $\partial_x f (x=1) - f_x(x=1)$')
    plt.semilogx()
    plt.semilogy()
    plt.savefig(f'figures/approximation_error.pdf', bbox_inches='tight', transparent=True)
    # plt.show()



if __name__ == '__main__':
    main()
