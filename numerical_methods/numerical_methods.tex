\section{Metodi di calcolo numerico}

\subsection{L'espansione in serie di Taylor}
Il calcolo numerico è uno degli strumenti fondamentali nello studio della fisica, in quanto permette di ottenere soluzioni di problemi analiticamente non risolvibili in modo numerico, e quindi avere nella maggior parte dei casi un risultato quantitativo che permette di studiare il fenomento senza doversi necessariamente fermare al limite analitico. In questo omdo integrazione, derivazione, ricerca di zeri, ma anche soluzione numerica alle equazioni alle derivate ordinarie o parziali può essere ottenuta. Al centro della maggior parte di queste tecniche numeriche abbiamo la serie di Taylor. Per ricordare abbiamo che, data una funzione $f(x)$, allora questa potrà essere espressa, almeno in un intorno del punto $x_0$ come la serie \[ f (x+x_0) = f(x_0) + \partial_i{f (x_0)}\qty(x - x_0) +  \partial_i^2 {f (x_0)}\qty(x - x_0)^2 + \dots,\] permettendo quindi di ottenere il valore della funzione (con un certo ordine di precisione, controllato dall'ordine entro cui si considera il troncamento della serie) anche per valori leggermente maggiori o minori di $x_0$, dove la funzione è nota, a patto di conoscere il valore in $x_0$ delle derivate della funzione\footnote{Non è necessario invece conoscere la forma analitica della funzione stessa, questo vedremo che tornerà molto utile nelle applicazioni per la soluzione di problemi numerici.}.

\subsection{Derivate prime e seconde}

Il primo problema che vogliamo affrontare è quello della derivazione. La definizione di derivata è data dal limite del rapporto incrementale, per cui \[ \partial_x f = \lim_{h\to0} \frac{f(x+h) -f(x)}{h} + \mathcal O (h) \] che segue direttamente dal troncamento al primo ordine dell'espansione di Taylor. Questo approccio potrebbe essere sufficente anche per procedere in modo numerico, imponendo $h$ sufficentemente piccola per \emph{indicare} il limite a zero. Però Si osserva che non è stabile, e inoltre risulta difficile in questo caso ottenere una condizione per definire quale deve essere la tolleranza che si deve avere su $h$. Infine è una approssimazione solo al primo ordine, quindi l'errore che si sta commettendo è $\mathcal O (h)$. 



Possiamo provare a troncare l'espansione di Taylor al terzo ordine, e vedere se si riesce a migliorare il risultato ottenuto. Considero quindi \[f (x+h) = f(x) + \partial_x{f (x)}h +  \partial_x^2 {f (x)}h^2 + \mathcal O (h^3), \] e considerata anche l'inversione di parità rispetto a $h$,\[f (x-h) = f(x) - \partial_x{f (x)}h +  \partial_x^2 {f (x)}h^2 - \mathcal O (h^3), \] allora possiamo ottenere, sottraendo membro a membro, \begin{equation}
    \partial_x f = \frac{f(x+h) - f(x-h)}{2h} + \mathcal O (h^2).
\end{equation}

\begin{wrapfigure}{r}{0.5\linewidth}
    \centering
    \includegraphics[]{figures/approximation_error.pdf}
    \caption{Si può osservare come la precisione sulla derivata, nell'esempio di $f(x) = x^3$, rispetto al valore di $h$, trova un minimo in $\sim 10^{-8}$ (si trascuri l'instabilità evidenziata per $h\lesssim10^{-8}$).}\label{fig:approximation_error}
\end{wrapfigure}

Osserviamo che la precisione è in generale migliorata, in quanto l'errore che si compie è dell'ordine di $h^2$. Potremmo pensare di andare allora a valori di $h$ piccolissimi, ma in questo modo non sembrerebbe possibile incontrare un limite inferiore. Dobbiamo però pensare che il calcolo nuerico della derivata prevede due contributi che possono imporre dei limiti sul valore di $h$. Da una parte abbiamo che le variabili sono rappresentate in \emph{floating point} per cui la precisione di calcolo è limitata. Questa componente di precisione diminuisce mano a mano che $h$ cresce. Dall'altra parte abbiamo invece che troncando la serie di Taylor abbiamo compiuto un errore di precisione. Questo contributo invece diminuisce on il diminuire di $h$. Possiamo osservare che questi due contributi si incontrano in un punto di stabilità, che nel caso in figura \ref{fig:approximation_error}, considerato l'esempio $f(x) = x^3$, si trova per $h\sim10^{-8}$. 

Potremmo allora pensare di diminuire di volta in volta $h$ per ottenere un valore ideale per cui non si ha una tolleranza per la derivata accettabile. 

Se ci poniamo in una condizione particolare, ovvero per cui abbiamo $f'(x) \sim f'''(x)$, allora possiamo provare a stimare quale sia il punto di incontro tra i due constributi e quindi quale sia il valore ideale di $h$. A causa della rappresentazione finita delle variabili floating point la precisione del
calcolo della differenza centrale \`e limitata, se consideriamo per il double $\epsilon = \num{e-16}$, allora \[\Delta_\epsilon f' = \Delta_\epsilon \frac{f(x+h) - f(x-h)}{2h} = \frac{2\Delta f}{2h} = \frac{\epsilon f(x)}{h}.\] A causa del troncamento per Taylor abbiamo \[\Delta_T = \frac{h^2}{3} f'''(x), \] e allora uguagliando i due contributi, e considerato che $f'(x) \sim f'''(x)$, allora otteniamo che \[h \simeq (3\epsilon) ^{1/3} \simeq \num{e-5}.\]

Per ottenere la derivata seconda possiamo procedere di nuovo considerando il metodo già utilizzato. Partiamo quindi dallo sviluppo di Taylor, di ordine quattro questa volta, avremo allora \[
    \begin{cases}
        f(x+h) = f(x) + hf'(x) + \frac{h^2}{2} f''(x) +\frac{h^3}{6} f'''(x) + \mathcal O (h^4)\\
        f(x-h) = f(x) - hf'(x) + \frac{h^2}{2} f''(x) -\frac{h^3}{6} f'''(x) + \mathcal O (h^4),
    \end{cases} 
\] ovvero che sommando membro a membro possiamo ottenere allora \[
    f(x+h) + f(x-h) = 2f(x) + h^2 f''(x),
\] ovvero \begin{equation}
    f''(x) = \frac{f(x+h) + f(x-h) - 2f(x)}{h^2}.
\end{equation}


\subsection{Integrazione numerica}
La definizione di integrale secondo Rienmann è un buon punto di partenza per definire numericamente l'estensione di tale concetto. 

Per la teoria di Riemann, date $n$ partizioni $\mathcal P$ in cui è diviso l'intervallo di integrazione $\mathcal D$, allora possiamo sefinire le somme inferiori e superiori come somme dei massimi e minimi per ogni partizione $m_k = \inf_{x\in[x_k, x_{k+1}]}f(x)$ e $M_k = \sup_{x\in[x_k, x_{k+1}]}f(x)$, ovvero \[\left\{
    \begin{aligned}
        s(\mathcal P, f) = \sum_{k=1}^n m_k(x_{k+1}-x_k)\\ 
        S(\mathcal P, f) = \sum_{k=1}^n M_k(x_{k+1}-x_k),
    \end{aligned}\right.
\] e dire che l'integrale esiste se e solo se \[
    \sup_{\mathcal P \in \wp} s(\mathcal P, f) = \inf_{\mathcal P \in \wp} s(\mathcal P, f) = \int_{\mathcal{D}} f(x) \dd x.  
\]

L'estensione naturale è quindi quella che prevede che sia diviso in un numero di intervalli sufficentemente grande il dominio $\mathcal D$ della funzione, e che sia valutata la funzione in ogni punto. In questo modo allora possiamo valutare l'area di ogni trapezio che si viene a formare e quindi ottenere una stima per il valore dell'integrale. L'area di tutti i trapezi, per cui ogni trapezio si trova tra $x_k$ e $x_{k+1}$, sarà data da \[\mathcal I = \sum_{k=0}^{n-1} \frac{w}{2} \qty[f(x_k) + f(x_{k+1})] \text{, dove } w=\frac{x_n-x_0}{n}.\]

Questo metodo è esatto per una retta, e in genere presenta errore proporzionale a $w^3$. Però così implementato risulta essere molto intensivo a livello computazionale, richiedendo per ogni $x_k$ di valutare la funzione due volte successive, quindi raddoppiando le richieste di calcolo. Possiamo però ovviare a questo problema sviluppando meglio la serie sopra presentata e ottenendo che \begin{equation}
    \mathcal I = \frac{w}{2}\qty[f(x_0) + f(x_n)] + w \sum_{k=1}^{n-2} f(x_k).
\end{equation} Abbiamo in questo caso interpolato tra i punti su $x$ considerando delle rette.

\paragraph*{Metodo di Simpson}
Possiamo migliorare l'algoritmo che abbiamo presentato qui considerando invece una interpolazione tra i punti discreti $x_k$ con una funzione polinomiale di grado più alto, come una parabola. Prr poter interpolare con una parabila, che è definita per tre punti, allora il numero di divisioni del dominio dovrà risultare pari, e il numero di \emph{nodi} dispari. La formula che si può ottenere è quindi \begin{equation}
    \mathcal I = \sum_{i=0}^{n-2} \frac{w}{6} \qty(f(x_i) + 4 f(x_{i+1}) + f(x_{i+2})), \text{ dove } w=\frac{x_n-x_0}{n}.
\end{equation}

In generale la scelta del numero di intervalli non è fissato a priori, ma è iterativo. Si parte da un numero preciso, non troppo piccolo, si valuta inizialmente l'integrale come somme su questa divisione. Si incrementa il numero di divisioni, procedendo a raddoppiarlo ad ogni passo, fino a raggiungere una precisione voluta. La precisione è in ogni caso valutata, similmente a come per le derivate, come la differenza tra l'iterazione precedente e l'iterazione successiva $\epsilon = |\mathcal I_{i+1} - \mathcal I_i|.$

\subsection{Implementazioni \emph{ready-to-use} in {\python} e \texttt{C++}}
In generale non è sempre richiesto di implementare gli algoritmi che si sono trovati partendo da zero, ma si possono sfruttare anche soluzioni già esistenti, a costo di perdere in alcuni casi in conoscenza di come il risultato è ottenuto, avendo in cambio però in alcuni casi una soluzione più efficace e veloce. Implementazioni di algoritmi per la differenziazione (derivazione) numerica sono implementati sia in {\python} che in \texttt{C++}. In {\python} abbiamo per esempio la libreria \emph{numpy} \cite{harrisArrayProgrammingNumPy2020}, che prevede il modulo \href{https://numpy.org/doc/stable/reference/generated/numpy.gradient.html#numpy.gradient}{\texttt{numpy.gradient(f, *varargs, axis=None, edge\_order=1)}}\footnote{\url{https://numpy.org/doc/stable/reference/generated/numpy.gradient.html}}, che presa una funzione scalare $f$ ne restituisce il gradiente (se la funzione è multidimensionale restituisce il gradiente in coordinate cartesiane). Un altro modulo che si può trovare e utilizzare è nella libreria \emph{scipy} \cite{virtanenSciPyFundamentalAlgorithms2020}, per cui la derivata, come differenza centrale, è calcolata con \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.misc.derivative.html#scipy.misc.derivative}{\texttt{scipy.misc.derivative(func, x0, dx=1.0, n=1, args=(), order=3)}}\footnote{\url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.misc.derivative.html}}. 

Altri metodi esistono implementati in altre diverse librerie, alcuni dei quali sfruttano anche metodi più fini e però computazionalmente più intensivi, di cui parleremo dopo. 

In \texttt{C++} possiamo invece trovare nella libreria {{ROOT}} \cite{brunRootprojectRootV62019} la classe \texttt{TF1} che possiede al suo interno il metodo \texttt{Double\_t TF1::Derivative(Double\_t x, Double\_t *params = nullptr, Double\_t eps = 0.001) const}\footnote{\url{https://root.cern.ch/doc/master/classTF1.html}} che permette di trovare la derivata prima di una funzione. Esistono anche parallelamente i metodi \emph{\texttt{Derivative2}} e \emph{\texttt{Derivative3}} che permettono di ottenere la derivata seconda e la derivata terza in un punto.

Anche per l'integrazione si possono trovare esempi validi di librerie che realizzano gli stesso calcoli. In particolar modo sempre rimanendo su python troviamo implementati sia il metodo semplice in numpy come \texttt{numpy.trapz(y, x=None, dx=1.0, axis=- 1)}\footnote{\url{https://numpy.org/doc/stable/reference/generated/numpy.trapz.html}} e sia il metodo di Simpson in scipy come \texttt{scipy.integrate.simpson(y, x=None, dx=1.0, axis=- 1, even='avg')}\footnote{\url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.simpson.html}}. 

Troviamo anche in questo caso alcuni metodi ulteriori e più raffinati, come il metodo di Romberg \cite{RombergMethod2022a, rombergVereinfachteNumerischeIntegration1955}, che è implementato in particolare in python come \texttt{scipy.integrate.romb(y, dx=1.0, axis=- 1, show=False)}\footnote{\url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.romb.html}}

Analogamente in \texttt{C++} possiamo trovare anche il metodo \texttt{Double\_t TF1::Integral(Double\_t a, Double\_t b, Double\_t epsrel = 1.e-12)}, che valuta l'integrale di una funzione unidimensionale.

\subsection{Metodo di bisezione e ricerca degli zeri di una funzione}

Il metodo di ricerca degli zeri di una funzione è tra i metodi più importanti che si utilizzino in calcolo numerico. Infatti oltre a permettere di individuare quando una funzione si annulla, può anche essere d'aiuto nell'identificare quando una funzione ha massimo (o minimo). Una funzione ha uno zero se è continua e assume, agli estremi di un intervallo valori discordi in segno. Con questa nozione il metodo di bisezione è piuttosto immediato, infatti definiti gli estremi dell'intervallo, il metodo è poi iterativo e si sviluppa su pochi punti successivi \begin{enumerate}
    \item Calcoliamo il punto medio dell'intervallo, o con una media normale, pesando ogni contributo a $1/2$, o migliorando la ricerca, implementando la \emph{golden search}, che prevede di porre il punto intermedio secondo il rapporto \[|x_k - x_{k+1}| / |x_{k+1} - x_\text{interval}| = 0.38197. \] In altri termini deve valere che se $b$ è la lunghezza di uno dei segmenti (quello tra $x_k$ e $x_\text{interval}$), e $a$ l'eltro segmento (che congiunge $x_\text{interval}$ con $x_{k+1}$), allora deve essere vero che \[\frac{a+b}{b} = \phi = \frac{1+\sqrt{5}}{2}.\]
    \item Valutiamo $f (x_\text{interval})$, se è zero allora la ricerca si ferma, altrimenti si procede.
    \item Si valuta allora se la funzione è discorde in segno nel sotto intervallo $x_k$, $x_\text{interval}$, ovvero se \[f(x_k)\cdot f(x_\text{interval})<0,\] se è vero allora si sposta $x_{k1}$ in $x_\text{interval}$, altrimenti si sposta $x_k$ in $x_\text{interval}$. Quindi si itera sui passaggi, fino a che non si trova uno zero, o la differenza tra gli estremi $x_k$ e $x_{k+1}$ non diventa inferiore alla precisione della variabile \emph{floating point}. In quest'utlimo caso il valore dello zero è dato dal valore di $x_\text{interval}$ dell'ultima iterazione.
\end{enumerate}

In tutti i casi di applicazione di algoritmi di calcolo numerico fondamentale risulta essere poter prevedere anche i casi di utilizzo errato, oppure i casi in cui esso potrebbe fallire. Nel caso specifico della ricerca di zeri alcune di queste possibilità possono essere espresse da \begin{enumerate}
    \item Intervallo in cui non si ha $x_k<x_{k+1}$, nel qual caso si può pensare di invertire gli estremi e poi procedere alla ricerca. 
    \item Intervallo in cui non si ha $f(x_k)f(x_{k+1})<0$, per cui si può direttamente emettere un valore di errore.
    \item Casi di funzioni continue con singolarità o casi di funzione discontinua.
    \item Errore nel braketing: può essere che la funzione abbia più di uno zero, in questo caso si avrà la necessità di trovare soluzioni alternative e \emph{ad-hoc} per il caso studiato.
\end{enumerate}

